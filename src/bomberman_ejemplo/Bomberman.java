package bomberman_ejemplo;

import java.util.Scanner;
import java.util.Random;
import java.util.Set;

import Core.Board;
import Core.Window;

/**
 * Joc de Bomberman amb recursivitat.
 * 
 * @author Iker Aguilera 
 * @version 1.0
 *
 */
public class Bomberman {
	static Random r = new Random();
	/**
	 *Variables estàtiques, countBomb són les bombes posades actualment,
	 *bombMax són el màxim de bombes que pots utilitzar alhora.
	 */
	static int countBomb = 0;
	static int bombMax = 2;
	
	/**
	 * Funció main. S'inicialitzen les variables i les matrius. S'executa el bucle de joc.
	 */
	public static void main(String[] args) throws InterruptedException {
		
		Board t = new Board();
		Window f = new Window(t);
		t.setActborder(false);
		t.setColorbackground(0xFFFFFF);
		
		/**
		 * Declaració de les dues matrius, la matriu principal on es situan els elements del joc, i la matrizBombas,
		 * on es col·loquen les bombes que utilitza el personatge.
		 */
		int[][] matriz = { 
				{1,1,1,1,1,1,1},
				{1,4,0,3,0,0,1},
				{1,0,2,0,2,0,1},
				{1,3,0,3,0,3,1},
				{1,0,2,0,2,0,1},
				{1,0,0,3,0,5,1},
				{1,1,1,1,1,1,1}
		};
		int[][] matrizBombas = new int[7][7];
		
		Posicio pbomberman = new Posicio(1,1);
		matriz[pbomberman.f][pbomberman.c]=4;
		printMatriz(matriz);
		

		Scanner sc = new Scanner(System.in);
		int count = 0;
		int countE = 0;
		boolean game = true;
		/**
		 * Bucle principal del joc. La variable instruccio agafa la tecla pressionada
		 * i s'executa la funció moure, per a moure al personatge.Després executa la funcion de moure enemics cada 5 bucles i
		 * la funció de netejar el camp i activar bomba cada 3 bucles.
		 * Finalment, llegeix la instrucció per a veure si es col·loca una bomba
		 * i després fa un print de la matriu principal.
		 */
		while(game) {
			Thread.sleep(200);
			Set<Character> instruccio = f.getKeysDown();
			pbomberman = moure(matriz, matrizBombas, pbomberman.f,pbomberman.c, instruccio);
			countE++;
			if (countE%5 == 0) {
				moureEnemic(matriz, matrizBombas);
			}
			count++;
			if (count%3 == 0) {
				neteja(matrizBombas);
				game = verEnemies(matriz);
				tick(matrizBombas,matriz);
			}
			if(instruccio.contains('z')) {
				if(countBomb < bombMax) {
					addBomba(pbomberman,matrizBombas);
				}
			}
			if(matrizBombas[pbomberman.f][pbomberman.c] == 9) {
				game = false;
			}
			printMatriz(matriz);
			printMatrizGrafica(matriz,matrizBombas,t,f);
			
		}
	}
	
	/**
	 * Aquesta és una petita funció per a comprovar si tots els enemics han estat eliminats de la matriu.
	 * Si és així retorna false perquè el joc acabi.
	 * 
	 * @param matriz
	 * @return game; Per veure si el bucle principal s'ha terminat o no.
	 */
	private static boolean verEnemies(int[][] matriz) {
		boolean game = false;
		for(int i=0;i<matriz.length;i++) {
			for(int k=0;k<matriz[0].length;k++) {
				if (matriz[i][k] == 5) {
					game = true;
				}
			}
		}
		return game;
	}


	/**
	 * Aquesta funció recorre la matriu de bombes per a eliminar les explosions.
	 * 
	 * @param matrizBombas
	 */
	private static void neteja(int[][] matrizBombas) {
		for(int i=0;i<matrizBombas.length;i++) {
			for(int k=0;k<matrizBombas[0].length;k++) {
				if (matrizBombas[i][k] == 9) {
					matrizBombas[i][k] = 0;
				}
			}
		}
	}

	/**
	 * En aquesta matriu es recorre la matrizBombas per a buscar una bomba,
	 * en cas de trobar-la, el temporitzador d'explosió baixa 1.
	 * En el cas que el temporitzador arribi a 10 s'executa la funció per a detonar la bomba.
	 * @param matrizBombas
	 * @param matriz
	 */
	private static void tick(int[][] matrizBombas, int[][] matriz) {
		for(int i=0;i<matrizBombas.length;i++) {
			for(int k=0;k<matrizBombas[0].length;k++) {
				if (matrizBombas[i][k] > 10) {
					matrizBombas[i][k]--;
					if (matrizBombas[i][k] == 10) {
						detonar(matrizBombas,matriz,i,k);
					}
				}
			}
		}
	}

	/**
	 * Aquesta és una funció recursiva que s'executa una vegada que una bomba sigui detonada
	 * i comprova tots els costats adjacents de la bomba.
	 * En cas que hi hagi una bomba al costat es torna a executar la funció.
	 * A més, en cas que una explosió coincideixi amb un mur o un enemic,
	 * s'eliminen aquests elements.
	 * @param matrizBombas
	 * @param matriz
	 * @param i, y de la matriu de l'explosió.
	 * @param k, x de la matriu de l'explosió.
	 */
	private static void detonar(int[][] matrizBombas, int[][] matriz,int i,int k) {
		countBomb--;
		matrizBombas[i][k] = 9;
		i++;
		if (matrizBombas[i][k] > 10) {
			detonar(matrizBombas,matriz,i,k);
		}
		matrizBombas[i][k] = 9;
		if (matriz[i][k] == 3 || matriz[i][k] == 5){
			matriz[i][k] = 0;
		}
		i-=2;
		if (matrizBombas[i][k] > 10) {
			detonar(matrizBombas,matriz,i,k);
		}
		matrizBombas[i][k] = 9;
		if (matriz[i][k] == 3 || matriz[i][k] == 5){
			matriz[i][k] = 0;
		}
		i++;
		k++;
		if (matrizBombas[i][k] > 10) {
			detonar(matrizBombas,matriz,i,k);
		}
		matrizBombas[i][k] = 9;
		if (matriz[i][k] == 3 || matriz[i][k] == 5){
			matriz[i][k] = 0;
		}
		k-=2;
		if (matrizBombas[i][k] > 10) {
			detonar(matrizBombas,matriz,i,k);
		}
		matrizBombas[i][k] = 9;
		if (matriz[i][k] == 3 || matriz[i][k] == 5){
			matriz[i][k] = 0;
		}
		k++;
	}

	/**
	 * Petita funció que col·loca una bomba en la posició del jugador.
	 * @param pbomberman
	 * @param matrizBombas
	 */
	private static void addBomba(Posicio pbomberman, int[][] matrizBombas) {
		matrizBombas[pbomberman.f][pbomberman.c]=16;
		countBomb++;
	}
	
	/**
	 * En aquesta funció es mira la instrucció donada pel jugador per a moure al personatge.
	 * Depèn de la tecla premuda comprovará que el personatge pugui col·locar-se en la casella,
	 * i si és així, s'elimina de la casella on estava i avança a la indicada.
	 * @param matriz
	 * @param matrizBombas
	 * @param h
	 * @param v
	 * @param instruccio
	 * @return La nova posició del jugador.
	 */
	private static Posicio moure(int[][] matriz, int[][] matrizBombas, int h, int v, Set<Character> instruccio) {
		int pj = matriz[h][v];
		if(instruccio.contains('w')) {
			matriz[h][v] = 0;
			
			h--;
			if(meSalgo(h,v,matriz) || matriz[h][v]!=0 || matrizBombas[h][v]!=0) {
				h++;
				matriz[h][v] = pj;
			}else {					
				matriz[h][v] = pj;
			}
			
		}
		else if(instruccio.contains('d')) {
			matriz[h][v] = 0;
			
			v++;
			if(meSalgo(h,v,matriz) || matriz[h][v]!=0 || matrizBombas[h][v]!=0) {
				v--;
				matriz[h][v] = pj;
			}else {

				matriz[h][v] = pj;
			}
			
		}
		else if(instruccio.contains('a')) {
			matriz[h][v] = 0;
			
			v--;
			if(meSalgo(h,v,matriz)  || matriz[h][v]!=0 || matrizBombas[h][v]!=0) {
				v++;
				matriz[h][v] = pj;
			}else {

				matriz[h][v] = pj;
			}
			
		}
		else if(instruccio.contains('s')) {
			matriz[h][v] = 0;
			
			h++;
			if(meSalgo(h,v,matriz) || matriz[h][v]!=0 || matrizBombas[h][v]!=0) {
				h--;
				matriz[h][v] = pj;
			}else {

				matriz[h][v] = pj;
			}
			
		}
		Posicio p = new Posicio(h, v);
		return p;
	}
	/**
	 * Aquesta funció és molt semblant a la de moure personatge.
	 * Primer es recorre la matriu a la recerca d'un enemic. En aquest cas, en comptes de donar una instrucció, genera un número aleatori i depèn del número es dirigeix a una direcció.
	 * En cas de no poder avançar en aquesta direcció es torna a realitzar el bucle.
	 * @param matriz
	 * @param matrizBombas
	 */
	private static void moureEnemic(int[][] matriz, int[][] matrizBombas) {
		boolean bucle = true;
		for(int h=0;h<matriz.length;h++) {
			for(int v=0;v<matriz[0].length;v++) {
				if (matriz[h][v] == 5) {
					while(bucle) {
						int pj = matriz[h][v];
						int m = r.nextInt(1,5);
						if(m == 1) {
							matriz[h][v] = 0;
							h--;
							if(meSalgo(h,v,matriz) || matriz[h][v]!=0 || matrizBombas[h][v]!=0) {
								h++;
								matriz[h][v] = pj;
							}else {					
								matriz[h][v] = pj;
								bucle = false;
							}
							
						}
						else if(m == 2) {
							matriz[h][v] = 0;
							v++;
							if(meSalgo(h,v,matriz) || matriz[h][v]!=0 || matrizBombas[h][v]!=0) {
								v--;
								matriz[h][v] = pj;
							}else {
	
								matriz[h][v] = pj;
								bucle = false;
							}
							
						}
						else if(m == 3) {
							matriz[h][v] = 0;
							v--;
							if(meSalgo(h,v,matriz)  || matriz[h][v]!=0 || matrizBombas[h][v]!=0) {
								v++;
								matriz[h][v] = pj;
							}else {
	
								matriz[h][v] = pj;
								bucle = false;
							}
							
						}
						else if(m == 4) {
							matriz[h][v] = 0;
							
							h++;
							if(meSalgo(h,v,matriz) || matriz[h][v]!=0 || matrizBombas[h][v]!=0) {
								h--;
								matriz[h][v] = pj;
							}else {
	
								matriz[h][v] = pj;
								bucle = false;
							}	
						}
					}
				}
			}
		}
		
	}
	

	/**
	 * Imprimeix la matriu principal de manera gràfica utilitzant les diferents imatges.
	 * @param matriz
	 * @param matrizBombas
	 * @param t
	 * @param f
	 */
	private static void printMatrizGrafica(int[][] matriz, int[][] matrizBombas, Board t, Window f) {
		String[] imatges = { "", "p1.png", "p1.png", "t1.png", "b1.png", "g1.png", "g1.png",
				"g1.png", "g1.png", "e.png","e.png","b2.png","b2.png","b2.png","b2.png","b2.png","b2.png","b2.png" };
		t.setSprites(imatges);
		int[][][] cosa = new int[2][7][7];
		cosa[0] = matriz;
		cosa[1] =matrizBombas;
		t.draw(cosa);
		
		
	}

	/**
	 * Simplement, imprimeix per consola la matriu indicada.
	 * @param matriz
	 */
	private static void printMatriz(int[][] matriz) {
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[0].length; j++) {
				System.out.print(matriz[i][j]+" ");
			}System.out.println();
		}
	}

	/**
	 * Funció per a veure si donada una posició si es surt de la matriu o no.
	 * @param h
	 * @param v
	 * @param matriz
	 * @return retorna un boolea, si es surt retorna true, si no retorna false.
	 */
	public static boolean meSalgo(int h, int v, int[][] matriz){	
		if(h<0||v<0||h>=matriz.length||v>=matriz[0].length) {
			return true;
		}else {
			return false;
		}
	}

}
